wg_rb_proj

Description:

# Introduction

  This is small ruby program to getting information about air asia flight fare. As long as airasia provide https://booking.airasia.com/Flight/Select? to public we can use it on operation GET REST request.
  
  The resource contains: 
     * exampleresponse.html (for development purpose only)

# To run this program

1. Open the ./lib/wgproj.rb file and change the parameter below: 

        # origin 
		o1 = "CGK"
		# destination
		d1 = "KUL"

		# keep it asis
		culture= "en-GB"

		# departure date
		dd1 = "2016-10-15"

		#adult passengger
		adt = "1"
		#child passengger
		chd = "0"

		inl = "0"
		s = "true"
		mon = "true"

		#currency
		cc = "IDR"
		c = "false"

2. If your current dir is wg_rb_proj run the wgproj.rb for example: 
    ruby ./lib/wgproj.rb
     Output example:
     
        "Request to https://booking.airasia.com/Flight/Select?o1=CGK&d1=KUL&culture=en-GB&dd1=2016-10-15&ADT=1&CHD=0&inl=0&s=true&montrue&cc=IDR&c=false"
        "please wait ...."
        fetching data from airasia
        rows_dark size=
        6
        rows_light size=
        6
        all_data size=
        12
        "=========================================="
        "=========================================="
        duration: 
         Flight duration : 2hla Lumpur (KUL)
        flight_code: 
        QZ  202
        depart_arrived_time: 
         (KUL)
        price: 
         (Adult) IDR
        ==========================================
        ==========================================
        duration: 
         Flight duration : 2hla Lumpur (KUL)
        flight_code: 
        QZ  200
        depart_arrived_time: 
         (KUL)
        price: 
         (Adult) IDR
        ==========================================
        ==========================================
        duration: 
         Flight duration : 2hla Lumpur (KUL)
        flight_code: 
        QZ  206
        depart_arrived_time: 
         (KUL)
        price: 
         (Adult) IDR
        ==========================================
        ==========================================
        duration: 
         Flight duration : 2hla Lumpur (KUL)
        flight_code: 
        AK  381
        depart_arrived_time: 
         (KUL)
        price: 
         (Adult)00 IDR
        ==========================================
        ==========================================
        duration: 
         Flight duration : 2h 10mumpur (KUL)
        flight_code: 
        AK  385
        depart_arrived_time: 
         (KUL)
        price: 
         (Adult) IDR
        ==========================================
        ==========================================
        duration: 
         Flight duration : 2hla Lumpur (KUL)
        flight_code: 
        AK  387
        depart_arrived_time: 
         (KUL)
        price: 
         (Adult) IDR
        ==========================================
        ==========================================
        the cheapest flight is: 
        {:flight_duration=>"\r QZ  202\r Jakarta (CGK) to Kuala Lumpur (KUL)\r Flight duration : 2h\r ", :flight_code=>"QZ  202", :depart_arrived_time=>"\r 06:25\r (CGK)\r 09:2(KUL)\r ",             :price=>"629000", :price_currency=>"\r 629,000 IDR\r (Adult)\r "}
