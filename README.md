Wg_proj

Description:

# Introduction

  This is small program to getting information about air asia flight fare. As long as airasia provide https://booking.airasia.com/Flight/Select? to public we can use it on operation GET REST request.
  
  The resource contains: 
     * params.properties
     * exampleresponse.html (for development purpose only)


# To run this program
  
  ## Run From IDE 
     
   1. Open params.properties under resource folder.  Put variable according your request in the file, you will find parameter like below
   
         # origin airport code ex: CGK(cengkareng jakarta), KUL(Kualalumpur malaysia)
         origin=CGK
               
         # destination airport code ex: CGK(cengkareng jakarta), KUL(Kualalumpur malaysia)
         destination=KUL
               
         # departure date in format yyyy-MM-dd ex: 2016-10-13
         departure.date=2016-10-15
               
         # number of adult passenger
         num.adult=1
               
         # number of child passenger
         num.child=0
               
         # number of invant passenger
         num.inl=0
               
         # currency code ex: (IDR -> Indonesian Rupiah),(MYR -> Malaysian Ringgit),(SGD)
         currency.code=IDR
        

      
   2. Open Main.java file ->right click->run
      
      The example output result in console:
        
         Searching air asia fare ticket for departure date: 2016-10-15
         sending request to : https://booking.airasia.com/Flight/Select?o1=CGK&d1=KUL&culture=en-Gb&dd1=2016-10-15&ADT=1&CHD=0&inl=0&s=true&montrue&cc=IDR&c=false
         responseCode: 200
         ======================================
         ======================================
         flight code: QZ 202
         departure & arrival time: 06:25 (CGK) 09:25 (KUL)
         duration: Jakarta (CGK) to Kuala Lumpur (KUL) Flight duration : 2h
         price: 759,000 IDR (Adult)
         ======================================
         ======================================
         flight code: AK 381
         departure & arrival time: 08:35 (CGK) 11:35 (KUL)
         duration: Jakarta (CGK) to Kuala Lumpur (KUL) Flight duration : 2h
         price: 889,000 IDR (Adult)
         fare seat: 5 seats left
         ======================================
         ======================================
         flight code: QZ 200
         departure & arrival time: 11:30 (CGK) 14:30 (KUL)
         duration: Jakarta (CGK) to Kuala Lumpur (KUL) Flight duration : 2h
         price: 629,000 IDR (Adult)
         ======================================
         ======================================
         flight code: AK 385
         departure & arrival time: 14:50 (CGK) 18:00 (KUL)
         duration: Jakarta (CGK) to Kuala Lumpur (KUL) Flight duration : 2h 10m
         price: 629,000 IDR (Adult)
         fare seat: 3 seats left
         ======================================
         ======================================
         flight code: QZ 206
         departure & arrival time: 18:35 (CGK) 21:35 (KUL)
         duration: Jakarta (CGK) to Kuala Lumpur (KUL) Flight duration : 2h
         price: 569,000 IDR (Adult)
         ======================================
         ======================================
         flight code: AK 387
         departure & arrival time: 20:30 (CGK) 23:30 (KUL)
         duration: Jakarta (CGK) to Kuala Lumpur (KUL) Flight duration : 2h
         price: 569,000 IDR (Adult)
         ======================================
         ======================================
         
         Process finished with exit code 0

     
  
  
  