package id.util;

import java.util.Comparator;
import java.util.Map;

/**
 * Created by harji on 10/7/16.
 */
public class MapComparator implements Comparator<Map<String, String>> {

    private final String key;

    public MapComparator(String key)
    {
        this.key = key;
    }

    public int compare(Map<String, String> o1, Map<String, String> o2) {
        if((o1==null) && (o2!=null)){
            return 1;
        }else if((o1!=null) && (o2==null)){
            return -1;
        }else if((o1==null) && (o2==null)){
            return 0;
        }
        String firstValue = o1.get(key);
        String secondValue = o2.get(key);
        return firstValue.compareTo(secondValue);

    }
}
