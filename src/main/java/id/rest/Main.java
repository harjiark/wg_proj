package id.rest;

import id.util.MapComparator;
import org.javalite.http.Get;
import org.javalite.http.Http;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.*;
import java.util.*;

/**
 * Created by harji on 10/4/16.
 */
public class Main {

    public static void main(String[] args) {


        try {

            Properties paramsProp = getParams("src/main/resources/params.properties");
            String o1 = (paramsProp.getProperty("origin") == null ? "CGK" : paramsProp.getProperty("origin"));
            String d1 = (paramsProp.getProperty("destination") == null ? "KUL" : paramsProp.getProperty("destination"));
            String culture = "en-Gb";
            String dd1 = (paramsProp.getProperty("departure.date") == null ? "2016-10-13" : paramsProp.getProperty("departure.date"));
            String ADT = (paramsProp.getProperty("num.adult") == null ? "1" : paramsProp.getProperty("num.adult"));
            String CHD = (paramsProp.getProperty("num.child") == null ? "0" : paramsProp.getProperty("num.child"));
            String inl = (paramsProp.getProperty("num.inl") == null ? "0" : paramsProp.getProperty("num.inl"));
            String s = "true";
            String mon = "true";
            String cc = (paramsProp.getProperty("currency.code") == null ? "IDR" : paramsProp.getProperty("currency.code"));
            String c = "false";

            String params = "o1=" + o1 + "&d1=" + d1 + "&culture=" + culture + "&dd1=" + dd1 + "&ADT=" + ADT + "&CHD=" + CHD + "&inl=" + inl + "&s=" + s + "&mon" + mon + "&cc=" + cc + "&c=" + c;
            String reqUrl = "https://booking.airasia.com/Flight/Select?"+params;

            System.out.println(" Searching air asia fare ticket for departure date: "+dd1 +" from "+o1+" to "+d1);

            System.out.println("sending request to : " + reqUrl);
            System.out.println("please wait ...");
            Get get = Http.get( reqUrl, 600000, 600000);
            int responseCode = get.responseCode();
            System.out.println("responseCode: " + responseCode);

            System.out.println("======================================");
            System.out.println("======================================");
            String htmlResponse = "";
//        Document doc = Jsoup.connect("https://booking.airasia.com/Flight/Select?o1=CGK&d1=KUL&culture=en-GB&dd1=2016-10-07&ADT=1&CHD=0&inl=0&s=true&mon=true&cc=IDR&c=false")
//                .cookie("auth", "token")
//                .timeout(60000)
//                .get();

            File input = new File("src/main/resources/exampleresponse.html");
//            Document doc = Jsoup.parse(input, "UTF-8");


            if (responseCode == 200) {
                htmlResponse = get.text();//(get.text() == null ? "" : get.text());
                List<HashMap<String,String>>listFlight = new ArrayList<HashMap<String, String>>();
                Document doc = Jsoup.parse(htmlResponse);

                for (Element table : doc.select("table[class=table avail-table]")) {

                    for (Element row : table.select("tr[class~=(fare-dark-row|fare-light-row)]")) {
                        String duration = row.select("div[class=carrier-hover-oneway-header] div[class!=carrier-hover-bold]").text();
                        String flightCode = row.select("div[class=carrier-hover-oneway-header] div[class=carrier-hover-bold]").text();
                        String departArrivedTime = row.select("table[class=avail-table-detail-table] td[class=avail-table-detail]").text();
                        String price = row.select("td[class$=avail-fare depart LF] div[class=avail-fare-price-container]").text();
                        String availFareSeat = row.select("div[class=avail-fare-seats]").text();

                        HashMap<String,String> flightMap = new HashMap<String, String>();
                        if (flightCode.length() > 0) {
                            System.out.println("flight code: " + flightCode);
                            flightMap.put("flightCode",flightCode);
                        }

                        if (departArrivedTime.length() > 0) {
                            System.out.println("departure & arrival time: " + departArrivedTime);
                            flightMap.put("departArrivedTime",departArrivedTime);
                        }

                        if (duration.length() > 0) {

                            System.out.println("duration: " + duration);
                            flightMap.put("duration",duration);
                        }

                        if (price.length() > 0) {
                            System.out.println("price: " + price);
                            flightMap.put("price_cc",price);
                            flightMap.put("price",price.replaceAll("[^\\d]", ""));
                        }

                        if(availFareSeat.length()>0){
                            System.out.println("fare seat: " + availFareSeat);
                            flightMap.put("availFareSeat",availFareSeat);
                        }

                        if(flightMap.size()>0) {
                            listFlight.add(flightMap);
                        }

                        System.out.println("======================================");
                    }

                    Collections.sort(listFlight,new MapComparator("price"));
                    System.out.println("the cheapest fare is: ");
                    System.out.println(Collections.singletonList(listFlight.get(0)));
                }


//            System.out.println("data: " + jsonData);
            } else {
                System.err.println("responseCode:" + responseCode);
                System.err.println("htmlResponse:" + htmlResponse);
            }

        } catch (Exception e) {
            e.printStackTrace();
//            System.err.println("error:" + );
        }
    }

    /**
     * Method to initiate properties file
     * @param propFileName
     * @return
     */
    private static Properties getParams(String propFileName) {

        InputStream inputStream = null;
        Properties prop = new Properties();
        try {
            inputStream = new FileInputStream(propFileName);
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

        } catch (Exception e) {
            System.err.println("error:" + e.getCause());
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return prop;
    }
}
