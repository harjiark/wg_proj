package str.replace;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;
//import org.apache.commons.io.filefilter.TrueFileFilter;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by harji on 10/19/16.
 */
public class ReplacerTools {
    private static String filePath = "";
    static int fileCount = 0;
    static String fileName = "conf/mapping.txt";
    static int lineNumber = 0;
    Set<File> files = new HashSet<File>();
    private static String folderPath = "";
    static Logger logger = Logger.getLogger(ReplacerTools.class);

    public static void main(String[] args) throws IOException {

        System.out.println("start replacing string according to mapping.txt");
        String osType = System.getProperty("os.name");
        Properties prop = new Properties();
        InputStream input = new FileInputStream("conf/replacer.properties");
        prop.load(input);
        String extensionstr = prop.getProperty("prop.extensions");
        System.out.println("prop.extensions: "+Arrays.toString(extensionstr.split(",")));

        System.out.println(osType);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        long startTime = System.currentTimeMillis();
        String startTimeString = formatter.format(startTime);

        System.out.println("startTime : " + startTimeString);

        PatternLayout layout = new PatternLayout("%d{yyyy-MM-dd-HH-mm-ss}%-5p %c{1}:%L - %m%n");

        RollingFileAppender fileAppender = new RollingFileAppender(layout, "./logs/replacer_" + startTimeString+ ".log");
        logger.getRootLogger().addAppender(fileAppender);

        System.out.println("start replacing string according to mapping.txt");
        folderPath = (osType.contains("Linux") ? (args.length == 0 ? "src/main/resources" : args[0]) :
                (args.length == 0 ? "src/main/resources" : args[0].replace("\\", File.separator)));

        System.out.println("folderPath:" + folderPath);

        File dirTobeScaned = new File(folderPath);
        String[] extensions = extensionstr.split(",");//new String[]{"xml"};

        List<File> fileList = (List<File>) FileUtils.listFiles(dirTobeScaned, extensions, true);
        System.out.println("fileUtils:" + fileList.size());

        List<String> linesList = FileUtils.readLines(new File(fileName));
        HashMap<String, String> mappStr = new HashMap<String, String>();

        for (String s : linesList) {
            if (s.length() != 0 && s.contains("|")) {
                String[] mapS = s.trim().split("\\|");
                mappStr.put(mapS[0].trim(), mapS[1].trim());
            }else{
                continue;
            }
        }


//        System.out.println("lineLisst:" + linesList.size());
//        System.out.println(Collections.singletonList(linesList));
//        System.out.println(Collections.singletonList(mappStr));


        String lineSeparator = (osType.contains("Linux") ? "\n" : "\n");

        for (File file : fileList) {
            BufferedReader br = new BufferedReader(new FileReader(file));
            StringBuffer sb = new StringBuffer();
            String line = null;
            logger.info("fileName: "+file.getAbsolutePath());

            while ((line = br.readLine()) != null) {
                for (Map.Entry<String, String> ent : mappStr.entrySet()) {
                    if (line.indexOf(ent.getKey()) != -1) {

                        logger.info("replace: "+ent.getKey()+" with : "+ent.getValue());
                        line = line.replace(ent.getKey(), ent.getValue());
                    }
                }
                sb.append(line).append(lineSeparator);
            }

            BufferedWriter bw = new BufferedWriter(new FileWriter(file));

            try {
                bw.write(sb.toString());
                bw.close();
            } catch (Exception io) {
                io.printStackTrace();
            }finally {
                if(bw!=null){
                    bw.close();
                }
            }

        }

        System.out.println("done ...");
    }


}
