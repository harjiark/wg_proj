Wg_proj

Description:

# Introduction

  This is small program to replacing string within xml file.

# To run this program
  1. Configure your mapping.txt string in map format for example:
        string_tobe_replaced|string_replacement
       for example:
          native://DN=cn=611,ou=Groups,dc=css,dc=hyperion,dc=com?GROUP|msad://OBJECTGUID=\70\43\03\E5\A6\40\FB\4E\8E\1C\11\B9\32\77\71\3E

  2. Run the jar with command :
       java -jar wg_proj.jar <directory_path_of_xml_file>
     example:
       java -jar wg_proj.jar C:\test\Contoh_File\FY2011
  
